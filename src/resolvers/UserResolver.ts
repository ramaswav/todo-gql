import { Resolver, Query, Mutation, Arg, Int, Args } from "type-graphql";
import { User } from "../entities/User";
import { CreateUserArgs } from "../types/CreateUserArgs";
import { getRepository } from "typeorm";

@Resolver(User)
export class UserResolver {
  @Query(() => [User])
  users(): Promise<User[]> {
    return getRepository(User)
      .createQueryBuilder("user")
      .leftJoinAndSelect("user.todos", "todos")
      .orderBy("user.id", "ASC")
      .addOrderBy("todo.id", "ASC")
      .getMany();
  }

  @Mutation(() => User)
  createUser(@Args() { name, email }: CreateUserArgs): Promise<User> {
    return User.create({ name, email, todos: [] }).save();
  }

  @Mutation(() => Boolean)
  async deleteUser(@Arg("ids", () => [Int]) ids: number[]): Promise<boolean> {
    try {
      await User.delete(ids);
      return true;
    } catch (err) {
      console.log(err);
      return false;
    }
  }
}
