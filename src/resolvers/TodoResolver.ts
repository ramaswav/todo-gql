import { Resolver, Query, Mutation, Args, Arg, Int } from "type-graphql";
import { Todo } from "../entities/Todo";
import { CreateTodoArgs } from "../types/CreateTodoArgs";
import { User } from "../entities/User";
import { UpdateTodoArgs } from "../types/UpdateTodoArgs";
import { getRepository } from "typeorm";
import { GetTodoArgs } from "../types/GetTodoArgs";

const relations: string[] = ["user"];

@Resolver(Todo)
export class TodoResolver {
  @Query(() => [Todo])
  todos(): Promise<Todo[]> {
    return Todo.find({ order: { id: "ASC" }, relations });
  }

  @Query(() => Todo)
  async todoOne(@Args() { id }: GetTodoArgs): Promise<Todo> {
    const todo = await getRepository(Todo)
      .createQueryBuilder("todo")
      .orderBy("todo.id", "ASC")
      .where("todo.createdBy = :id", { id: id })
      .getOne();
    if (!todo) throw new Error("Todo not found for user");

    return todo;
  }

  @Mutation(() => Todo)
  async createTodo(@Args() args: CreateTodoArgs): Promise<Todo> {
    const todo = await Todo.create(args).save();
    const user = await getRepository(User)
      .createQueryBuilder("user")
      .orderBy("user.id", "ASC")
      .where("user.id = :createdBy", { createdBy: todo.createdBy })
      .getOne();

    if (!user) throw new Error("User not found");
    todo.user = user;

    return todo;
  }

  @Mutation(() => Todo)
  async updateTodo(@Args() { id, ...args }: UpdateTodoArgs): Promise<Todo> {
    const todo = await Todo.findOne({ where: { id }, relations });
    if (!todo) throw new Error("Todo not found");
    todo.description = args.description;
    todo.title = args.title;
    todo.completed = args.completed;
    return todo.save();
  }

  @Mutation(() => Boolean)
  async deleteTodo(@Arg("ids", () => [Int]) ids: number[]): Promise<boolean> {
    try {
      await Todo.delete(ids);
      return true;
    } catch (err) {
      console.log(err);
      return false;
    }
  }
}
