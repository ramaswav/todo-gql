let request = require("request");

const CreateTodo = (req: any, res: any) => {
  let options = {
    method: "POST",
    url: "http://localhost:4000/graphql",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      query: `mutation createTodo($title: String!, $description: String!, $createdBy: Int! ) {  createTodo(title: $title, description: $description, createdBy: $createdBy) {  ...TodoFields  }}
      fragment TodoFields on Todo {  id, title }`,
      variables: req.body,
    }),
  };
  request(options, function (error: any, response: any) {
    if (error) throw new Error(error);
    const { data } = JSON.parse(response.body);
    if (data) {
      res.send({ id: data.createTodo.id, title: data.createTodo.title });
    } else {
      res.status(400).send("failed bad data.");
    }
  });
};

export default CreateTodo;
