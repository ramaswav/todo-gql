let request = require("request");

const CreateUser = (req: any, res: any) => {
  let options = {
    method: "POST",
    url: "http://localhost:4000/graphql",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      query: `mutation createUser($name: String!, $email: String!) {  createUser(name: $name, email: $email) {  ...UserFields  }}
fragment UserFields on User {  id }`,
      variables: req.body,
    }),
  };
  request(options, function (error: any, response: any) {
    if (error) throw new Error(error);
    const { data } = JSON.parse(response.body);
    if (data) {
      res.send({ userId: data.createUser.id });
    } else {
      res.status(400).send("failed bad data.");
    }
  });
};

export default CreateUser;
