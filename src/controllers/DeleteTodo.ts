let request = require("request");
const DeleteTodo = (req: any, res: any) => {
  let options = {
    method: "POST",
    url: "http://localhost:4000/graphql",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      query: `mutation DeleteTodo($ids: [Int!]!) { deleteTodo(ids: $ids) }`,
      variables: { ids: [parseInt(req.params.id)] },
    }),
  };
  try {
    request(options, function (error: any, _response: any) {
      if (error) throw new Error(error);
      res.send({ deleted: req.params.id });
    });
  } catch (e) {
    res.status(400).send("failed bad data.");
  }
};
export default DeleteTodo;
