let request = require("request");

export const ListTodo = (req: any, res: any) => {
  let options = {
    method: "POST",
    url: "http://localhost:4000/graphql",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      query: `query todoOne($id: Float!) { todoOne(id: $id)  {...TodoFields } }
        fragment TodoFields on Todo { id title description createdOn completed createdBy }`,
      variables: { id: parseInt(req.params.id) },
    }),
  };

  request(options, function (error: any, response: any) {
    if (error) throw new Error(error);
    const { data } = JSON.parse(response.body);
    if (data) {
      res.send(data.todoOne);
    } else {
      res.status(400).send("failed bad data.");
    }
  });
};

export const ListTodos = (_req: any, res: any) => {
  let options = {
    method: "POST",
    url: "http://localhost:4000/graphql",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      query: `query todos { todos {...TodoFields } }
    fragment TodoFields on Todo { id title description createdOn completed createdBy }`,
      variables: {},
    }),
  };
  request(options, function (error: any, response: any) {
    if (error) throw new Error(error);
    const { data } = JSON.parse(response.body);

    if (data) {
      res.send(data);
    } else {
      res.status(400).send("failed bad data.");
    }
  });
};
