let request = require("request");

const UpdateTodo = (req: any, res: any) => {
  let options = {
    method: "POST",
    url: "http://localhost:4000/graphql",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      query: `mutation updateTodo($title: String!, $description: String!, $id: Int!, $completed: Boolean! ) {  updateTodo(title: $title, description: $description, id: $id, completed: $completed) {  ...TodoFields  }}
        fragment TodoFields on Todo { id title description createdBy completed}`,
      variables: req.body,
    }),
  };
  request(options, function (error: any, response: any) {
    if (error) throw new Error(error);
    const { data } = JSON.parse(response.body);
    if (data) {
      res.send(data.updateTodo);
    } else {
      res.status(400).send("failed bad data.");
    }
  });
};

export default UpdateTodo;
