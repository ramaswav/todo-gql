import { ArgsType, Field, Int } from "type-graphql";
import { IsInt, IsBoolean, Min, IsString, Length } from "class-validator";

@ArgsType()
export class UpdateTodoArgs {
  @Field(() => Int)
  @IsInt()
  @Min(1)
  id: number;

  @Field()
  @IsBoolean()
  completed: boolean;

  @Field()
  @IsString()
  @Length(1, 255)
  title: string;

  @Field()
  @IsString()
  @Length(1, 255)
  description: string;
}
