import { ArgsType, Field } from "type-graphql";
import { IsEmail, IsString, Length } from "class-validator";

@ArgsType()
export class CreateUserArgs {
  @Field()
  @IsString()
  @Length(1, 255)
  name: string;

  @Field()
  @IsEmail()
  @Length(1, 255)
  email: string;
}
