import { ArgsType, Field, Int } from "type-graphql";
import { IsString, Length, IsInt } from "class-validator";

@ArgsType()
export class CreateTodoArgs {
  @Field()
  @IsString()
  @Length(1, 255)
  title: string;

  @Field()
  @IsString()
  @Length(1, 255)
  description: string;

  @Field(() => Int)
  @IsInt()
  createdBy: number;
}
