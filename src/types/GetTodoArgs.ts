import { ArgsType, Field } from "type-graphql";
import { IsNumber } from "class-validator";

@ArgsType()
export class GetTodoArgs {
  @Field()
  @IsNumber()
  id: number;
}
