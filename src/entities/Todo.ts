import {
  Entity,
  BaseEntity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  ManyToOne,
} from "typeorm";
import { Field, Int, ObjectType } from "type-graphql";
import { User } from "./User";

@ObjectType()
@Entity("todo")
export class Todo extends BaseEntity {
  @Field(() => Int)
  @PrimaryGeneratedColumn()
  readonly id: number;

  @Field()
  @Column()
  title: string;

  @Field()
  @Column()
  description: string;

  @Field()
  @CreateDateColumn()
  readonly createdOn: Date;

  @Field()
  @Column({ default: false })
  completed: boolean;

  @Field()
  @Column()
  readonly createdBy: number;

  @Field(() => User)
  @ManyToOne(() => User, (user) => user.todos, { onDelete: "CASCADE" })
  user: User;
}
