import "reflect-metadata";
import { createConnection } from "typeorm";
import { buildSchema } from "type-graphql";
import { ApolloServer } from "apollo-server-express";
import express from "express";
import CreateUser from "./controllers/CreateUser";
import CreateTodo from "./controllers/CreateTodo";
import DeleteTodo from "./controllers/DeleteTodo";
import { ListTodo, ListTodos } from "./controllers/ListTodo";
import UpdateTodo from "./controllers/UpdateTodo";

(async () => {
  const [schema] = await Promise.all([
    buildSchema({
      resolvers: [`${__dirname}/resolvers/*Resolver.[tj]s`],
    }),
    createConnection(),
  ]);

  const server = new ApolloServer({ schema });

  const app = express();
  app.use(express.json());

  // User
  app.post("/createUser", (req: any, res: any) => CreateUser(req, res));

  // Todo's
  app.get("/allTodos", (req: any, res: any) => ListTodos(req, res));
  app.get("/todo/:id", (req: any, res: any) => ListTodo(req, res));
  app.post("/createTodo", (req: any, res: any) => CreateTodo(req, res));
  app.put("/updateTodo", (req: any, res: any) => UpdateTodo(req, res));
  app.delete("/deleteTodo/:id", (req: any, res: any) => DeleteTodo(req, res));

  server.applyMiddleware({ app });

  const PORT = process.env.PORT || 4000;
  app.set("port", PORT);
  app.listen(PORT, () => {
    console.log(
      `Server running at http://localhost:${PORT}${server.graphqlPath}`
    );
  });
})();
