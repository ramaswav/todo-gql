# Tech Stack

1. Node.JS
2. Express
3. Typescript
4. GraphQL
5. Apollo Server
6. PostgreSQL
7. TypeORM
8. Type GraphQL
9. React
10. GraphQL Request
11. Hosted on GCP VM

# API Endpoints

1. GET http://35.223.245.174:4000/allTodos
2. GET http://35.223.245.174:4000/todo/8
3. POST http://35.223.245.174:4000/createUser
4. PUT http://35.223.245.174:4000/updateTodo
5. DELETE http://35.223.245.174:4000/deleteTodo/9
6. GraphQL playground at http://35.223.245.174:4000/graphql


## Todo
1. Add Input validation for REST enpdoints
2. Add Test cases
